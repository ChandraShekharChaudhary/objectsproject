let Obj = require('../obj.cjs');
let defaults=require('../defaults.cjs');

let defaultProps1={age: 98};
console.log(defaults(Obj, defaultProps1));

let defaultProps2={year : 1998};
console.log(defaults(Obj, defaultProps2));

let defaultProps3='string';
console.log(defaults(Obj, defaultProps3));

let Objc=25;
console.log(defaults(Objc, defaultProps2));