//This file is for testing of  the values problem on diffrent argument passing.

let values = require('../values.cjs');
let Obj = require('../obj.cjs');

console.log(values(Obj));
console.log(values({5:12, 6:'ram', 'sohan':15}));
console.log(values());
console.log(values('shekhar'));
console.log(values(50));
console.log(values(84.65));
console.log(values([]));
console.log(values([{5:12, 6:'ram', 'sohan':15}]));
console.log(values("Mount blue"));
console.log(values({}));
