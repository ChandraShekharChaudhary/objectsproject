//let Obj =require('../obj.cjs');
let Obj={name: 'Bruce Wayne', age: 36, location: 'Gotham', year:1985}
let mapObject = require('../mapObject.cjs');


function addSuffix(value, key) {
  return typeof value === 'string' ? value + '-' + key : value;
}
const result = mapObject(Obj, addSuffix);
console.log(result);


function doubleAge(value, key) {
    return key==='age' ? value*2 : value;
}
const result2=mapObject(Obj, doubleAge);
console.log(result2);

