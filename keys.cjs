//const Obj= require('./obj.cjs');

function keys(Obj){   //created keys function for return 
    if(typeof Obj !== 'object' || Obj === null){
        return [];
    }
    if (Array.isArray(Obj)) {
        const keyList = [];
        for (let index = 0; index < Obj.length; index++) {
          keyList.push(String(index));
        }
        return keyList;
    }

    let keyList=[];
    for(let key in Obj){
        let strKey=String(key);
        keyList.push(strKey);
    }
    return keyList;
}

module.exports = keys;
