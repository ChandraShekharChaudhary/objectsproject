
function values(Obj){  //In this function if we  pass an object as argument then it will return value of all keys in the form of an array. 
    if(typeof Obj !== 'object' || Array.isArray(Obj)){
        return [];
    }
    let valueList = [];
    for(key in Obj){
       valueList.push(Obj[key]);
    }
    return valueList;
}

module.exports = values;
