function defaults(obj, defaultProps) {
    if(typeof obj !== 'object' || Array.isArray(obj)){
        return {};
    }

    if(typeof defaultProps !== 'object' || Array.isArray(obj)){
        return obj;
    }

    for (const key in defaultProps) {
      if (defaultProps.hasOwnProperty(key) && obj[key] === undefined) {
        obj[key] = defaultProps[key];
      }
    }
    return obj;
  }

  module.exports=defaults;