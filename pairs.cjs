// let obj=require('./obj.cjs');

function pairs(obj) {
    if(typeof obj !== 'object' || Array.isArray(obj)){
        return [];
    }
    let pairList=[];
    for(let key in obj){
        pairList.push([key , obj[key]]);
    }
    return pairList;
}

module.exports=pairs;