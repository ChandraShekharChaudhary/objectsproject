function invert(obj) {
    if(typeof obj !== 'object' || Array.isArray(obj)){
        return {};
    }
    let newObj={};
    for(let key in obj){
        let k=obj[key];
        let v=key;
        newObj[k]=v;
    }
    return newObj;
}

module.exports=invert;